namespace RailroadsCommon;

public static class Guard
{
    public static void NotNull(object? obj) {
        if (obj == null) {
            throw new ArgumentNullException(nameof(obj));
        }
    }
}
