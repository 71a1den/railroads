namespace RailroadsCommon;

public static class FillCalculator
{
    public static Point[] Calculate(Park selectedPark) {
        List<Point> points = new();
        foreach (var railway in selectedPark.Railways) {
            if(railway.Segments != null) {
                foreach (var segment in railway.Segments) {
                    if (!points.Contains(segment.Begin)) {
                        points.Add(segment.Begin);
                    }
                    if (!points.Contains(segment.End)) {
                        points.Add(segment.End);
                    }
                }
            }
        }

        var orderedByX = points.OrderBy(p => p.X);
        var orderedByY = points.OrderBy(p => p.Y);

        double minX = orderedByX.First().X;
        double maxX = orderedByX.Last().X;

        double minY = orderedByY.First().Y;
        double maxY = orderedByY.Last().Y;

        var rightPoints = orderedByX.Where(p => p.X == maxX).OrderBy(p => p.Y);
        var topPoints = orderedByY.Where(p => p.Y == maxY).OrderByDescending(p => p.X);
        var leftPoints = orderedByX.Where(p => p.X == minX).OrderByDescending(p => p.Y);
        var bottomPoints = orderedByY.Where(p => p.Y == minY).OrderBy(p => p.X);

        var result = new List<Point>();

        AddEndPoints(result, rightPoints);
        AddEndPoints(result, topPoints);
        AddEndPoints(result, leftPoints);
        AddEndPoints(result, bottomPoints);

        return result.ToArray();
    }
    private static void AddEndPoints(List<Point> result, IEnumerable<Point> points) {
        var first = points.First();
        var last = points.Last();

        if (!result.Contains(first)) { 
            result.Add(first);
        }
        if (!result.Contains(last)) {
            result.Add(last);
        }
    }
}
