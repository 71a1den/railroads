namespace RailroadsCommon;

internal class TestPark1 : Park
{
    public static Railway[] PredefinedRailways = new Railway[] {
        new("Путь А", new Segment[] {
            TestStation.PredefinedSegments[0],
            TestStation.PredefinedSegments[3],
            TestStation.PredefinedSegments[5],
            TestStation.PredefinedSegments[6],
            TestStation.PredefinedSegments[7],
            TestStation.PredefinedSegments[8],
        }),
        new("Путь Б", new Segment[] {
            TestStation.PredefinedSegments[9],
            TestStation.PredefinedSegments[10],
            TestStation.PredefinedSegments[11],
            TestStation.PredefinedSegments[12]
        }),
        new("Путь В", new Segment[] {
            TestStation.PredefinedSegments[24],
            TestStation.PredefinedSegments[25],
            TestStation.PredefinedSegments[26]
        })
    };

    public TestPark1()
        : base("Тестовый парк 1", PredefinedRailways) {
    }
}
