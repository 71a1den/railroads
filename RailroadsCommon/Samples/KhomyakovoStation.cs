namespace RailroadsCommon;

public class KhomyakovoStation : Station
{
    public static Segment[] PredefinedSegments = new[] {
        new Segment(1, 0, 1, 2), new Segment(1, 2, 0, 4), new Segment(0, 4, 0, 6), new Segment(1, 2, 1, 8), new Segment(0, 6, 1, 8), new Segment(1, 8, 1, 10), new Segment(1, 10, 2, 12), new Segment(2, 12, 4, 14), new Segment(4, 14, 6, 16),
        new Segment(4, 0, 4, 8), new Segment(4, 8, 4, 10), new Segment(4, 10, 5, 12), new Segment(5, 12, 6, 16), new Segment(6, 16, 9, 18),
        new Segment(5, 0, 5, 4), new Segment(5, 4, 5, 6), new Segment(5, 6, 4, 8), new Segment(6, 0, 6, 2), new Segment(6, 2, 5, 4),
        new Segment(7, 0, 7, 2), new Segment(7, 2, 8, 4), new Segment(8, 0, 8, 4), new Segment(8, 4, 8, 6), new Segment(8, 6, 10, 8),
        new Segment(10, 2, 10, 8), new Segment(10, 8, 10, 14), new Segment(10, 14, 9, 18),
        new Segment(9, 18, 11, 19), new Segment(11, 19, 17, 21), new Segment(11, 19, 12, 21), new Segment(17, 21, 32, 24), new Segment(32, 24, 34, 26),
        new Segment(7, 18, 12, 21), new Segment(12, 21, 15, 22), new Segment(15, 22, 34, 26), new Segment(15, 22, 17, 21), new Segment(34, 26, 37, 27), new Segment(37, 27, 39, 29),
        new Segment(1, 20, 16, 24), new Segment(16, 24, 39, 29), new Segment(16, 24, 18, 26), new Segment(39, 29, 50, 31),
        new Segment(0, 21, 18, 26), new Segment(18, 26, 22, 27), new Segment(22, 27, 49, 32),
        new Segment(22, 27, 23, 28), new Segment(22, 27, 23, 29), new Segment(23, 28, 37, 31), new Segment(23, 29, 36, 32), new Segment(36, 32, 37, 31), new Segment(37, 31, 43, 32),
        new Segment(43, 32, 45, 34), new Segment(45, 34, 45, 36), new Segment(45, 36, 44, 37), new Segment(44, 37, 33, 37), new Segment(33, 37, 31, 38), new Segment(31, 38, 31, 40), new Segment(31, 40, 36, 43), new Segment(31, 40, 32, 42), new Segment(32, 42, 23, 40), new Segment(32, 42, 36, 43), new Segment(36, 43, 38, 44), new Segment(38, 44, 39, 47)
    };
    public static Park[] PredefinedParks = new[] {
        new OilDepotPark()
    };

    public KhomyakovoStation()
        : base("Хомяково", PredefinedSegments) {
    }
}
