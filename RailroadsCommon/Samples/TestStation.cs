namespace RailroadsCommon;

public class TestStation : Station
{
    public static Segment[] PredefinedSegments = new[] {
        new Segment(1, 0, 1, 2), new Segment(1, 2, 0, 4), new Segment(0, 4, 0, 6), new Segment(1, 2, 1, 8), new Segment(0, 6, 1, 8), new Segment(1, 8, 1, 10), new Segment(1, 10, 2, 12), new Segment(2, 12, 4, 14), new Segment(4, 14, 6, 16),
        new Segment(4, 0, 4, 8), new Segment(4, 8, 4, 10), new Segment(4, 10, 5, 12), /*new Segment(5, 12, 6, 16),*/ new Segment(6, 16, 9, 18),
        new Segment(5, 0, 5, 4), new Segment(5, 4, 5, 6), new Segment(5, 6, 4, 8), new Segment(6, 0, 6, 2), new Segment(6, 2, 5, 4),
        new Segment(7, 0, 7, 2), new Segment(7, 2, 8, 4), new Segment(8, 0, 8, 4), new Segment(8, 4, 8, 6), new Segment(8, 6, 10, 8),
        new Segment(10, 2, 10, 8), new Segment(10, 8, 10, 14), new Segment(10, 14, 9, 18), new Segment(9, 18, 11, 19)
    };
    public static Park[] PredefinedParks = new Park[] {
        new TestPark1(),
        new TestPark2()
    };

    public TestStation()
        : base("Тестовая станция", PredefinedSegments) {
    }
}
