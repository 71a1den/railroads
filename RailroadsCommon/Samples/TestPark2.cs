namespace RailroadsCommon;

internal class TestPark2 : Park
{
    public static Railway[] PredefinedRailways = new Railway[] {
        new("Путь В", new Segment[] {
            TestStation.PredefinedSegments[9],
            TestStation.PredefinedSegments[10],
            TestStation.PredefinedSegments[11]
        }),
        new("Путь Г", new Segment[] {
            TestStation.PredefinedSegments[23],
            TestStation.PredefinedSegments[24],
            TestStation.PredefinedSegments[25]
        })
    };

    public TestPark2()
        : base("Тестовый парк 2", PredefinedRailways) {
    }
}
