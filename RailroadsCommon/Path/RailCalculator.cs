using RailroadsCommon.Path;

namespace RailroadsCommon;

public class RailCalculator
{
    private readonly List<PointVector> neighbours = new();
    private readonly int maxSteps;
    private readonly IBinaryHeap<Point, PointVector> frontier;
    private readonly HashSet<Point> ignoredPositions;
    private readonly Station station;
    private readonly IDictionary<Point, Point> links;

    public RailCalculator(Station station, int maxSteps = int.MaxValue, int initialCapacity = 0) {
        Guard.NotNull(station);
        this.station = station;

        if (maxSteps <= 0) {
            throw new ArgumentOutOfRangeException(nameof(maxSteps));
        }

        if (initialCapacity < 0) {
            throw new ArgumentOutOfRangeException(nameof(initialCapacity));
        }

        this.maxSteps = maxSteps;
        frontier = new BinaryHeap<Point, PointVector>(v => v.Position, initialCapacity);
        ignoredPositions = new HashSet<Point>(initialCapacity);
        links = new Dictionary<Point, Point>(initialCapacity);
    }

    public bool Calculate(Point start, Point target, out List<Point> points, out List<Segment> segments) {
        station.Initialize();

        points = new List<Point>();
        segments = new List<Segment>();
        if (!GenerateNodes(start, target)) {
            return false;
        }

        points.Add(target);
        while (links.TryGetValue(target, out var next)) {
            points.Add(next);
            var segment = station.FindSegment(target, next);
            segments.Add(segment);
            target = next;
        }

        return true;
    }

    private bool GenerateNodes(Point start, Point target) {
        frontier.Clear();
        ignoredPositions.Clear();
        links.Clear();

        frontier.Enqueue(new PointVector(start, target, 0));
        var step = 0;
        while (frontier.Count > 0 && step++ <= maxSteps) {
            PointVector current = frontier.Dequeue();
            ignoredPositions.Add(current.Position);

            if (current.Position.Equals(target)) {
                return true;
            }

            GenerateFrontierNodes(current);
        }

        return false;
    }
    private void GenerateFrontierNodes(PointVector current) {
        Tuple<Point, double>[] values = GetAvailablePoints(current.Position, neighbours, current);
        foreach (PointVector newNode in neighbours) {
            if (ignoredPositions.Contains(newNode.Position)) {
                continue;
            }

            if (!frontier.TryGet(newNode.Position, out PointVector existingNode)) {
                frontier.Enqueue(newNode);
                links[newNode.Position] = current.Position;
            }
            else if (newNode.MovingDistance < existingNode.MovingDistance) {
                frontier.Modify(newNode);
                links[newNode.Position] = current.Position;
            }
        }
    }
    private Tuple<Point, double>[] GetAvailablePoints(Point point, List<PointVector> buffer, PointVector parent) {
        var availablePoints = new List<Tuple<Point, double>>();
        foreach (var availablePoint in station.Points[point]) {
            double segmentLength = new Segment(point, availablePoint).Length;
            availablePoints.Add(new Tuple<Point, double>(availablePoint, segmentLength));

            Point nodePosition = parent.Position;
            double traverseDistance = parent.MovingDistance + segmentLength;
            var pointVector = new PointVector(availablePoint, nodePosition, traverseDistance);
            buffer.Add(pointVector);
        }
        return availablePoints.ToArray();
    }
}
