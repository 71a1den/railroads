namespace RailroadsCommon;

public readonly struct PointVector : IComparable<PointVector>
{
    private static readonly double Sqr = Math.Sqrt(2);

    public Point Position { get; }
    public double MovingDistance { get; }
    internal double Cost { get; }

    public PointVector(Point position, Point target, double movingDistance) {
        Position = position;
        MovingDistance = movingDistance;

        var vector = position - target;
        int linearSteps = Math.Abs(Math.Abs(vector.Y) - Math.Abs(vector.X));
        int diagonalSteps = Math.Max(Math.Abs(vector.Y), Math.Abs(vector.X)) - linearSteps;
        var eurDistance = linearSteps + Sqr * diagonalSteps;

        Cost = movingDistance + eurDistance;
    }
    public int CompareTo(PointVector other) {
        return Cost.CompareTo(other.Cost);
    }
}
