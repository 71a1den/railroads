namespace RailroadsCommon;

public class Park
{
    public string Name { get; }
    public IEnumerable<Railway> Railways { get; }

    public Park(string name, IEnumerable<Railway> railways) {
        Guard.NotNull(railways);
        Railways = railways;
        Name = name;
    }
    public override string ToString() {
        return Name;
    }
}
