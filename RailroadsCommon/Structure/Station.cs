namespace RailroadsCommon;

public class Station
{
    private readonly Dictionary<Point, List<Point>> points = new();

    public List<Segment> Segments { get; }
    public IEnumerable<Point> AllPoints => points.Keys;
    public string Name { get; private set; }
    internal Dictionary<Point, List<Point>> Points => points;

    public Station(string name, IEnumerable<Segment> segments) {
        Name = name;
        Segments = new List<Segment>(segments);
        points = new Dictionary<Point, List<Point>>();
    }

    public void Initialize() {
        points.Clear();
        foreach (var segment in Segments) {
            Point begin = segment.Begin;
            Point end = segment.End;

            FillPoints(begin, end);
            FillPoints(end, begin);
        }
    }
    internal Segment FindSegment(Point point1, Point point2) {
        return Segments.First(s => s.Begin == point1 && s.End == point2 || s.Begin == point2 && s.End == point1);
    }

    private void FillPoints(Point begin, Point end) {
        if (points.TryGetValue(begin, out var neighbours)) {
            if (!neighbours.Contains(end)) {
                neighbours.Add(end);
            }
        }
        else {
            points.Add(begin, new List<Point>() { end });
        }
    }
}
