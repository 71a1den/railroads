namespace RailroadsCommon;

public readonly struct Segment : IComparable<Segment>, IEquatable<Segment>
{
    private readonly int hashCode;

    public Point Begin { get; }
    public Point End { get; }
    public double Length { get; }

    public Segment(Point begin, Point end) {
        Begin = begin;
        End = end;
        hashCode = HashCode.Combine(begin, end);

        int xD = Math.Abs(Begin.X - End.X);
        int yD = Math.Abs(Begin.Y - End.Y);
        Length = Math.Sqrt(Math.Pow(xD, 2) + Math.Pow(yD, 2));
    }
    public Segment(int x1, int y1, int x2, int y2)
        : this(new Point(x1, y1), new Point(x2, y2)) {
    }
    public int CompareTo(Segment other) {
        return Length.CompareTo(other.Length);
    }
    public bool Equals(Segment other) {
        Guard.NotNull(other);
        return Begin.Equals(other.Begin) && End.Equals(other.End);
    }
    public override bool Equals(object? obj) {
        Guard.NotNull(obj);
        return obj is Segment segment && Equals(segment);
    }
    public override int GetHashCode() {
        return hashCode;
    }
    public override string ToString() {
        return $"{Begin} - {End}";
    }
}
