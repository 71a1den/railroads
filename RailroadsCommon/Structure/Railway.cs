namespace RailroadsCommon;

public class Railway
{
    public List<Segment> Segments { get; }
    public string Name { get; }

    public Railway(string name, Segment[] segments) {
        Guard.NotNull(name);
        Guard.NotNull(segments);
        Name = name;
        Segments = new List<Segment>(segments);
    }
}
