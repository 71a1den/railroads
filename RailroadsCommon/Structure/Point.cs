namespace RailroadsCommon; 

public readonly struct Point : IEquatable<Point> {
    public static Point operator +(Point a, Point b) => new(a.X + b.X, a.Y + b.Y);
    public static Point operator -(Point a, Point b) => new(a.X - b.X, a.Y - b.Y);
    public static bool operator ==(Point left, Point right) => left.Equals(right);
    public static bool operator !=(Point left, Point right) => !left.Equals(right);

    private readonly int hashCode;

    public int X { get; }
    public int Y { get; }

    public Point(int x, int y) {
        X = x;
        Y = y;
        hashCode = HashCode.Combine(X, Y);
    }
    public bool Equals(Point other) {
        Guard.NotNull(other);
        return X.Equals(other.X) && Y.Equals(other.Y);
    }
    public override bool Equals(object? obj) {
        Guard.NotNull(obj);
        return obj is Point point && Equals(point);
    }
    public override int GetHashCode() {
        return hashCode;
    }
    public override string ToString() {
        return $"({X}, {Y})";
    }
}
