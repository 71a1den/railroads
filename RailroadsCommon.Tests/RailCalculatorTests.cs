using RailroadsCommon;

namespace RailroadsTests;

[TestFixture]
public class RailCalculatorTests
{
    private Segment[] park1;

    [SetUp]
    public void SetUp() {
        park1 = new[] { new Segment(1, 0, 1, 2), new Segment(1, 2, 0, 4), new Segment(0, 4, 0, 6), new Segment(1, 2, 1, 8), new Segment(0, 6, 1, 8), new Segment(1, 8, 1, 10) };
    }

    [Test]
    public void Path1Test() {
        var start = new Point(1, 0);
        var end = new Point(1, 2);
        var station = new Station("TestStation", park1);

        var railCalculator = new RailCalculator(station);
        railCalculator.Calculate(start, end, out var result, out _);

        Assert.That(result.Count, Is.EqualTo(2));
        Assert.That(result[0], Is.EqualTo(new Point(1, 2)));
        Assert.That(result[1], Is.EqualTo(new Point(1, 0)));
    }

    [Test]
    public void Path2Test() {
        var start = new Point(1, 0);
        var end = new Point(0, 4);

        var station = new Station("TestStation", park1);

        var railCalculator = new RailCalculator(station);
        railCalculator.Calculate(start, end, out var result, out _);

        Assert.That(result.Count, Is.EqualTo(3));

        Assert.That(result[0], Is.EqualTo(new Point(0, 4)));
        Assert.That(result[1], Is.EqualTo(new Point(1, 2)));
        Assert.That(result[2], Is.EqualTo(new Point(1, 0)));
    }

    [Test]
    public void Path3Test() {
        var start = new Point(0, 6);
        var end = new Point(1, 0);

        var station = new Station("TestStation", park1);

        var railCalculator = new RailCalculator(station);
        railCalculator.Calculate(start, end, out var result, out _);

        Assert.That(result.Count, Is.EqualTo(4));

        Assert.That(result[0], Is.EqualTo(new Point(1, 0)));
        Assert.That(result[1], Is.EqualTo(new Point(1, 2)));
        Assert.That(result[2], Is.EqualTo(new Point(0, 4)));
        Assert.That(result[3], Is.EqualTo(new Point(0, 6)));
    }
}
