using RailroadsCommon.Path;

namespace RailroadsTests;

[TestFixture]
public class BinaryHeapTests
{
    [Test]
    public void EnqueueDequeueTest() {
        var heap = new BinaryHeap<int, string>(a => a.Length);

        heap.Enqueue("train");
        heap.Enqueue("pass");
        heap.Enqueue("ticket");
        Assert.That(heap.Count, Is.EqualTo(3));

        heap.Modify("brain");

        Assert.IsTrue(heap.TryGet(6, out var text));
        Assert.IsTrue(heap.TryGet(5, out text));
        Assert.IsTrue(heap.TryGet(4, out text));

        var a = heap.Dequeue();
        Assert.That(a, Is.EqualTo("pass"));

        a = heap.Dequeue();
        Assert.That(a, Is.EqualTo("brain"));

        a = heap.Dequeue();
        Assert.That(a, Is.EqualTo("ticket"));

        a = heap.Dequeue();
        Assert.That(a, Is.Null);
        Assert.That(heap.Count, Is.EqualTo(0));
    }
}
