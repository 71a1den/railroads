using RailroadsCommon;

namespace RailroadsTests;

[TestFixture]
public class FillCalculatorTests
{
    private Segment[] park1;
    private Segment[] park2;

    [SetUp]
    public void SetUp() {
        park1 = new[] { new Segment(0, 0, 5, 0), new Segment(5, 0, 10, 0), new Segment(8, 6, 6, 6), new Segment(6, 6, 4, 6) };
        park2 = new[] { new Segment(1, 0, 1, 2), new Segment(1, 2, 0, 4), new Segment(0, 4, 0, 6), new Segment(1, 2, 1, 8), new Segment(0, 6, 1, 8), new Segment(1, 8, 1, 10) };
    }

    [Test]
    public void Fill1Test() {
        var fillPoints = FillCalculator.Calculate(new Park("Тестовый парк", new Railway[] {
            new ("Тестовый путь1", new Segment[] {
                park2[0],
                park2[3],
                park2[5]
            }),
        }));

        Assert.That(fillPoints.Length, Is.EqualTo(2));
        Assert.That(fillPoints[0], Is.EqualTo(new Point(1, 0)));
        Assert.That(fillPoints[1], Is.EqualTo(new Point(1, 10)));
    }

    [Test]
    public void Fill2Test() {
        var fillPoints = FillCalculator.Calculate(new Park("Тестовый парк", new Railway[] {
            new ("Тестовый путь1", new Segment[] {
                park1[2],
                park1[3]
            }),
            new ("Тестовый путь2", new Segment[] {
                park1[0],
                park1[1]
            })
        }));

        Assert.That(fillPoints.Length, Is.EqualTo(4));
        Assert.That(fillPoints[0], Is.EqualTo(new Point(10, 0)));
        Assert.That(fillPoints[1], Is.EqualTo(new Point(8, 6)));
        Assert.That(fillPoints[2], Is.EqualTo(new Point(4, 6)));
        Assert.That(fillPoints[3], Is.EqualTo(new Point(0, 0)));
    }

    [Test]
    public void Fill3Test() {
        var fillPoints = FillCalculator.Calculate(new Park("Тестовый парк", new Railway[] {
            new ("Тестовый путь1", new Segment[] {
                park2[0],
                park2[3],
                park2[5],
            }),
            new ("Тестовый путь2", new Segment[] {
                park2[2]
            })
        }));

        Assert.That(fillPoints.Length, Is.EqualTo(4));
        Assert.That(fillPoints[0], Is.EqualTo(new Point(1, 0)));
        Assert.That(fillPoints[1], Is.EqualTo(new Point(1, 10)));
        Assert.That(fillPoints[2], Is.EqualTo(new Point(0, 6)));
        Assert.That(fillPoints[3], Is.EqualTo(new Point(0, 4)));
    }
}
