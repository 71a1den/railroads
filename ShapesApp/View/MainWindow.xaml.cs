﻿using ShapesApp.ViewModel;
using System.Windows;
using System.Windows.Shapes;

namespace ShapesApp {
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        void Line_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            MainViewModel viewModel = (MainViewModel)DataContext;

            PointViewModel pointViewModel = (sender as Shape)?.DataContext as PointViewModel;
            if(pointViewModel != null)
                viewModel.SelectPoint(pointViewModel);
        }
    }
}