using System.Windows.Data;
using System.Windows.Media;

namespace ShapesApp;

[ValueConversion(typeof(bool), typeof(bool))]
public class PointStateBrushConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
        var state = (SelectionState) value;
        switch (state) {
            case SelectionState.Route:
                return new SolidColorBrush(Colors.LightGreen);
            case SelectionState.Selected:
                return new SolidColorBrush(Colors.Orange);
            default:
                return new SolidColorBrush(Colors.LightGray);
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
        throw new NotSupportedException();
    }
}
