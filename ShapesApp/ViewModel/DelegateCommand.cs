using System.Windows.Input;

namespace ShapesApp.ViewModel;

internal class DelegateCommand : ICommand
{
    private readonly Action executeAction;
    private readonly Func<bool> canExecuteDelegate;

    event EventHandler ICommand.CanExecuteChanged {
        add => CommandManager.RequerySuggested += value;
        remove => CommandManager.RequerySuggested -= value;
    }

    public DelegateCommand(Action executeAction, Func<bool> canExecuteDelegate = null) {
        this.executeAction = executeAction;
        this.canExecuteDelegate = canExecuteDelegate;
    }
    bool ICommand.CanExecute(object parameter) {
        return canExecuteDelegate?.Invoke() ?? true;
    }
    void ICommand.Execute(object parameter) {
        executeAction?.Invoke();
    }
}
