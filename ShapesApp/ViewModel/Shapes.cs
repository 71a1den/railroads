using System.Windows.Media;

namespace ShapesApp.ViewModel;

public abstract class ShapeViewModel : BindableBase
{
    protected static double CalculateCoordinate(int coordinate, double ratio) {
        if (coordinate == 0) {
            return ratio;
        }
        else {
            return (coordinate * ratio) + ratio;
        }
    }

    private Brush fill;
    public Brush Fill {
        get => fill;
        set {
            fill = value;
            RaisePropertyChanged();
        }
    }
    private Brush stroke;
    public Brush Stroke {
        get => stroke;
        set {
            stroke = value;
            RaisePropertyChanged();
        }
    }

    private double top;
    public double Top {
        get => top;
        set {
            top = value;
            RaisePropertyChanged();
        }
    }
    private double left;
    public double Left {
        get => left;
        set {
            left = value;
            RaisePropertyChanged();
        }
    }
    private string name;
    public string Name {
        get => name;
        set {
            name = value;
            RaisePropertyChanged();
        }
    }
    private double opacity;
    public double Opacity {
        get => opacity;
        set {
            opacity = value;
            RaisePropertyChanged();
        }
    }

    public ShapeViewModel() {
        Fill = Brushes.LightGray;
        Stroke = Brushes.DarkBlue;
        Name = GetType().Name;
    }
}