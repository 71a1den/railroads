using RailroadsCommon;

namespace ShapesApp.ViewModel;

public class SegmentViewModel : ShapeViewModel
{
    private double x1;
    private double x2;
    private double y1;
    private double y2;
    private SelectionState state;

    public double X1 {
        get => x1;
        set {
            x1 = value;
            RaisePropertyChanged();
        }
    }
    public double X2 {
        get => x2;
        set {
            x2 = value;
            RaisePropertyChanged();
        }
    }

    public double Y1 {
        get => y1;
        set {
            y1 = value;
            RaisePropertyChanged();
        }
    }
    public double Y2 {
        get => y2;
        set {
            y2 = value;
            RaisePropertyChanged();
        }
    }

    public SelectionState State {
        get => state;
        set {
            state = value;
            RaisePropertyChanged();
        }
    }

    public SegmentViewModel(Segment segment, double ratio, double layoutHeight) {
        X1 = CalculateCoordinate(segment.Begin.X, ratio);
        Y1 = layoutHeight - CalculateCoordinate(segment.Begin.Y, ratio);
        X2 = CalculateCoordinate(segment.End.X, ratio);
        Y2 = layoutHeight - CalculateCoordinate(segment.End.Y, ratio);
        state = SelectionState.Unselected;
    }
}
