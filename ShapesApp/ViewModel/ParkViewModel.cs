using System.Windows;
using System.Windows.Media;

namespace ShapesApp.ViewModel;

public class ParkViewModel : ShapeViewModel
{
    public PointCollection Points { get; }

    public ParkViewModel(IEnumerable<Point> points) {
        Points = new PointCollection(points);
        Points.Freeze();
        Fill = Brushes.LightGreen;
    }
}
