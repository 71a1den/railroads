using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ShapesApp.ViewModel;

public class BindableBase : INotifyPropertyChanged
{
    protected void RaisePropertyChanged([CallerMemberName] string propertyName = null) {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    public event PropertyChangedEventHandler PropertyChanged;
}
