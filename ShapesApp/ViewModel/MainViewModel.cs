using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using RailroadsCommon;
using Point = RailroadsCommon.Point;

namespace ShapesApp.ViewModel;

public class MainViewModel : BindableBase
{
    private static readonly Segment[] StationSegments = new[] {
        new Segment(1, 0, 1, 2), new Segment(1, 2, 0, 4), new Segment(0, 4, 0, 6), new Segment(1, 2, 1, 8), new Segment(0, 6, 1, 8), new Segment(1, 8, 1, 10), new Segment(1, 10, 2, 12), new Segment(2, 12, 4, 14), new Segment(4, 14, 6, 16),
        new Segment(4, 0, 4, 8), new Segment(4, 8, 4, 10), new Segment(4, 10, 5, 12), new Segment(5, 12, 6, 16), new Segment(6, 16, 9, 18),
        new Segment(5, 0, 5, 4), new Segment(5, 4, 5, 6), new Segment(5, 6, 4, 8), new Segment(6, 0, 6, 2), new Segment(6, 2, 5, 4),
        new Segment(7, 0, 7, 2), new Segment(7, 2, 8, 4), new Segment(8, 0, 8, 4), new Segment(8, 4, 8, 6), new Segment(8, 6, 10, 8),
        new Segment(10, 2, 10, 8), new Segment(10, 8, 10, 14), new Segment(10, 14, 9, 18),
        new Segment(9, 18, 11, 19), new Segment(11, 19, 17, 21), new Segment(11, 19, 12, 21), new Segment(17, 21, 32, 24), new Segment(32, 24, 34, 26),
        new Segment(7, 18, 12, 21), new Segment(12, 21, 15, 22), new Segment(15, 22, 34, 26), new Segment(15, 22, 17, 21), new Segment(34, 26, 37, 27), new Segment(37, 27, 39, 29),
        new Segment(1, 20, 16, 24), new Segment(16, 24, 39, 29), new Segment(16, 24, 18, 26), new Segment(39, 29, 50, 31),
        new Segment(0, 21, 18, 26), new Segment(18, 26, 22, 27), new Segment(22, 27, 49, 32),
        new Segment(22, 27, 23, 28), new Segment(22, 27, 23, 29), new Segment(23, 28, 37, 31), new Segment(23, 29, 36, 32), new Segment(36, 32, 37, 31), new Segment(37, 31, 43, 32),
        new Segment(43, 32, 45, 34), new Segment(45, 34, 45, 36), new Segment(45, 36, 44, 37), new Segment(44, 37, 33, 37), new Segment(33, 37, 31, 38), new Segment(31, 38, 31, 40), new Segment(31, 40, 36, 43), new Segment(31, 40, 32, 42), new Segment(32, 42, 23, 40), new Segment(32, 42, 36, 43), new Segment(36, 43, 38, 44), new Segment(38, 44, 39, 47)
    };
    private static readonly Park[] StationParks = new[] {
        new Park("Парк рудного комбината", new Railway[] {
            new ("Путь Е", new Segment[] {
                StationSegments[54],
                StationSegments[55],
                StationSegments[56],
                StationSegments[57],
                StationSegments[61],
                StationSegments[62]
            }),
            new ("Путь Ж", new Segment[] {
                StationSegments[59],
                StationSegments[60],
                StationSegments[61]
            })
        }),
        new Park("Парк станции", new Railway[] {
            new ("Путь Г", new Segment[] {
                StationSegments[42],
                StationSegments[43],
                StationSegments[44]
            }),
            new ("Путь Д", new Segment[] {
                StationSegments[38],
                StationSegments[39],
                StationSegments[41]
            })
        }),
        new Park("Парк Нефтебазы", new Railway[] {
            new ("Путь А", new Segment[] {
                StationSegments[0],
                StationSegments[3],
                StationSegments[5],
                StationSegments[6],
                StationSegments[7],
                StationSegments[8]
            }),
            new ("Путь Б", new Segment[] {
                StationSegments[9],
                StationSegments[10],
                StationSegments[11],
                StationSegments[12]
            }),
            new ("Путь В", new Segment[] {
                StationSegments[24],
                StationSegments[25],
                StationSegments[26]
            }),
        })
    };

    private readonly Dictionary<Point, PointViewModel> pointViewModels = new();
    private readonly Dictionary<Segment, SegmentViewModel> segmentViewModels = new();
    private readonly List<PointViewModel> selectedPoints = new();
    private readonly Station station;
    private List<Point> routePoints;
    private List<Segment> routeSegments;

    private double windowWidth;
    public double WindowWidth {
        get => windowWidth;
        set {
            windowWidth = value;
            RaisePropertyChanged();
        }
    }

    private double windowHeight;
    public double WindowHeight {
        get => windowHeight;
        set {
            windowHeight = value;
            RaisePropertyChanged();
        }
    }

    public ICommand ClearParkCommand { get; }
    public ICommand CalcPathCommand { get; }

    public ObservableCollection<Park> AvailableParks { get; }
    public ObservableCollection<ShapeViewModel> Shapes { get; }
    public string StationName => station.Name;

    private ParkViewModel selectedParkViewModel;
    private Park selectedPark;
    public Park SelectedPark {
        get => selectedPark;
        set {
            if (selectedParkViewModel != null) {
                Shapes.Remove(selectedParkViewModel);
            }
            SelectPark(SelectionState.Unselected);
            selectedPark = value;
            SelectPark(SelectionState.Selected);
            if (selectedPark != null) {
                var fillPoints = FillCalculator.Calculate(selectedPark);
                selectedParkViewModel = new ParkViewModel(fillPoints.Select(p => new System.Windows.Point(pointViewModels[p].X, pointViewModels[p].Y))) { Opacity = 0.5 };
                Shapes.Add(selectedParkViewModel);
            }
            RaisePropertyChanged();
        }
    }

    public MainViewModel() {
        WindowWidth = 1440;
        WindowHeight = 768;

        station = new Station("Хомяково", StationSegments);
        station.Initialize();

        AvailableParks = new ObservableCollection<Park>(StationParks);
        Shapes = CreateLayout();

        ClearParkCommand = new DelegateCommand(ClearPark, CanClearPark);
        CalcPathCommand = new DelegateCommand(CalcPath, CanCalcPath);

        Observable
            .FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
            h => PropertyChanged += h,
            h => PropertyChanged -= h)
            .Where(e => e.EventArgs.PropertyName == nameof(SelectedPark))
            .Subscribe((e) => {

            });
        ;
    }

    public void SelectPoint(PointViewModel pointViewModel) {
        ClearRoute();
        switch (pointViewModel.State) {
            case SelectionState.Route:
            case SelectionState.Selected:
                pointViewModel.State = SelectionState.Unselected;
                selectedPoints.Remove(pointViewModel);
                break;
            case SelectionState.Unselected:
                if (selectedPoints.Count > 1) {
                    selectedPoints[0].State = SelectionState.Unselected;
                    selectedPoints.RemoveAt(0);
                }

                pointViewModel.State = SelectionState.Selected;
                selectedPoints.Add(pointViewModel);
                break;
            default:
                break;
        }
    }

    protected virtual bool CanClearPark() {
        return SelectedPark != null;
    }
    protected virtual void ClearPark() {
        SelectedPark = null;
    }
    protected virtual bool CanCalcPath() {
        return selectedPoints.Count == 2;
    }
    protected virtual void CalcPath() {
        ClearRoute();
        if (selectedPoints.Count == 2) {
            var calculator = new RailCalculator(station);
            if (calculator.Calculate(selectedPoints[0].Point, selectedPoints[1].Point, out routePoints, out routeSegments)) {
                if (routePoints.Count == 0) {
                    MessageBox.Show("Между выбранными точками нельзя проложить маршрут", "Внимание", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else {
                    selectedPoints.Clear();
                    foreach (var routePoint in routePoints) {
                        pointViewModels[routePoint].State = SelectionState.Route;
                    }
                    foreach (var routeSegment in routeSegments) {
                        segmentViewModels[routeSegment].State = SelectionState.Route;
                    }
                }
            }
            else {
                MessageBox.Show("Не удалось построить маршрут", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        else {
            MessageBox.Show("Для расчета маршрута нужно выбрать две точки", "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }

    private void ClearRoute() {
        if (routePoints != null) {
            foreach (var routePoint in routePoints) {
                pointViewModels[routePoint].State = SelectionState.Unselected;
            }
            routePoints = null;
        }
        if (routeSegments != null) {
            foreach (var routeSegment in routeSegments) {
                segmentViewModels[routeSegment].State = SelectionState.Unselected;
            }
            routePoints = null;
        }
    }
    private void SelectPark(SelectionState state) {
        if (selectedPark != null) {
            foreach (var railway in selectedPark.Railways) {
                foreach (var segment in railway.Segments) {
                    segmentViewModels[segment].State = state;
                }
            }
        }
    }
    private ObservableCollection<ShapeViewModel> CreateLayout() {
        double layoutWidth = WindowWidth - 200;
        double layoutHeight = WindowHeight - 60;

        var orderedByX = station.AllPoints.OrderBy(p => p.X);
        var orderedByY = station.AllPoints.OrderBy(p => p.Y);

        int minX = orderedByX.First().X;
        int lastX = orderedByX.Last().X;
        int diffX = lastX - minX;

        int minY = orderedByY.First().Y;
        int lastY = orderedByY.Last().Y;
        int diffY = lastY - minY;
        var ratio = Math.Min(layoutWidth / (diffX + 2), layoutHeight / (diffY + 2));

        var shapes = new ObservableCollection<ShapeViewModel>();
        foreach (Segment segment in station.Segments) {
            var vm = new SegmentViewModel(segment, ratio, layoutHeight) { Opacity = 1 };
            shapes.Add(vm);
            segmentViewModels.Add(segment, vm);
        }
        foreach (Point point in station.AllPoints) {
            var vm = new PointViewModel(point, ratio, layoutHeight) { Opacity = 1 };
            shapes.Add(vm);
            pointViewModels.Add(point, vm);
        }
        return shapes;
    }
}
