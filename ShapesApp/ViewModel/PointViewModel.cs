using RailroadsCommon;

namespace ShapesApp.ViewModel;

public class PointViewModel : ShapeViewModel
{
    private double x;
    private double y;
    private double width;
    private SelectionState state;
    private string tooltip;

    public double X {
        get => x;
        set {
            x = value;
            RaisePropertyChanged();
        }
    }
    public double Y {
        get => y;
        set {
            y = value;
            RaisePropertyChanged();
        }
    }
    public double Width {
        get => width;
        set {
            width = value;
            RaisePropertyChanged();
        }
    }
    public SelectionState State {
        get => state;
        set {
            state = value;
            RaisePropertyChanged();
        }
    }
    public string Tooltip {
        get => tooltip;
        set {
            tooltip = value;
            RaisePropertyChanged();
        }
    }
    public Point Point { get; }

    public PointViewModel(Point point, double ratio, double layoutHeight) {
        Point = point;
        X = CalculateCoordinate(point.X, ratio);
        Y = layoutHeight - CalculateCoordinate(point.Y, ratio);
        state = SelectionState.Unselected;
        tooltip = point.ToString();
    }
}
