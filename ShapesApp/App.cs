using System;
using System.Windows;

namespace ShapesApp;

public class App : Application
{
    private readonly MainWindow mainWindow;

    public App(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }
    protected override void OnStartup(StartupEventArgs e) {
        mainWindow.Show();
        base.OnStartup(e);
    }
}
