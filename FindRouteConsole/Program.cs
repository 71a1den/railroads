using RailroadsCommon;

namespace FindRouteConsole;

public class Program
{
    private static void Main(string[] args) {
        Process();
        Console.ReadLine();
    }
    private static void Process() {
        var station = new TestStation();

        int counter = 1;
        Console.WriteLine($"Список участков станции {station.Name}: ");
        foreach (var segment in station.Segments) {
            Console.WriteLine($"{counter++}. [{segment}]");
        }
        Console.WriteLine();

        Console.WriteLine($"Выберите номер стартового участка");
        if (!ParseIndex(station.Segments.Count, out int startIndex)) {
            Console.WriteLine($"Номер указан некорректно");
            return;
        }

        Console.WriteLine($"Выберите номер конечного участка");
        if (!ParseIndex(station.Segments.Count, out int endIndex)) {
            Console.WriteLine($"Номер указан некорректно");
            return;
        }

        var calculator = new RailCalculator(station);
        if (calculator.Calculate(station.Segments[startIndex - 1].Begin, station.Segments[endIndex - 1].End, out var points, out var path)) {
            Console.WriteLine("Кратчайший путь найден: ");
            for (var i = path.Count - 1; i >= 0; i--) {
                Console.WriteLine($"[{path[i]}]");
            }
        }
        else {
            Console.WriteLine($"Пути между участками под номерами {startIndex} и {endIndex} не существует");
        }
    }
    private static bool ParseIndex(int count, out int index) {
        if (!int.TryParse(Console.ReadLine(), out index) || index < 1 || index > count) {
            return false;
        }
        else {
            return true;
        }
    }
}
