using RailroadsCommon;

namespace FillParksConsole;

public class Program
{
    private static void Main(string[] args) {
        foreach (var park in TestStation.PredefinedParks) {
            Console.WriteLine($"{park.Name}: [{park.Railways.Select(r => r.Name).Aggregate((i, j) => i + ", " + j)}]");

            Console.WriteLine($"Список вершин, описывающих парк: {FillCalculator.Calculate(park).Select(p => p.ToString()).Aggregate((i, j) => i + ", " + j)}");
        }

        Console.ReadLine();
    }
}
